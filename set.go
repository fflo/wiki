package wiki

import "bytes"

// Set represents a set of identifiers in the content
// of an article.
// The implementation does not guarantee that the
// entries in the set are unique.
type Set [][]byte

// Get returns true if the set contains the given entry.
func (s Set) Get(key string) bool {
	keyb := []byte(key)
	return s.GetIf(func(cat []byte) bool {
		return bytes.Equal(keyb, cat)
	})
}

// Each iterates over all entries in the set.
func (s Set) Each(f func([]byte)) {
	for _, cat := range s {
		f(cat)
	}
}

// GetIf returns true if the callback function
// returns true for any entry in the set.
func (s Set) GetIf(f func([]byte) bool) bool {
	for _, cat := range s {
		if f(cat) {
			return true
		}
	}
	return false
}

// appendTrimmed appends an trimmed byte slice to the set.
// If the trimmed byte slice is empty, nothing is apended.
func appendTrimmed(set Set, bs []byte) Set {
	trimmed := bytes.Trim(bs, " \t\n\r")
	if len(trimmed) == 0 {
		return set
	}
	return append(set, trimmed)
}
