package wiki

import (
	"errors"
	"io"
	"os"
	"strings"
	"testing"
)

func ta(path string, f func(*Article) error) error {
	is, err := os.Open(path)
	if err != nil {
		panic("cannot read test file: " + path)
	}
	return EachArticle(is, f)
}

func TestEmpty(t *testing.T) {
	t.Parallel()
	n := 0
	err := ta("testdata/empty.xml", func(*Article) error {
		n++
		return nil
	})
	if err != nil {
		t.Fatalf("got error: %s", err)
	}
	if n != 0 {
		t.Fatalf("got %d; expected 0", n)
	}
}

func TestInvalidXML(t *testing.T) {
	t.Parallel()
	err := ta("testdata/invalid.xml", func(*Article) error {
		return nil
	})
	if err == nil {
		t.Fatalf("expected error; got nil")
	}
}

func TestCallbackEOF(t *testing.T) {
	t.Parallel()
	var n int
	err := ta("testdata/special.xml", func(*Article) error {
		n++
		return io.EOF
	})
	if err != nil {
		t.Fatalf("got error: %s", err)
	}
	if n != 1 { // exactly one call of function
		t.Fatalf("expected %d; got %d", 1, n)
	}
}

func TestInvalidCallback(t *testing.T) {
	t.Parallel()
	err := ta("testdata/test.xml", func(*Article) error {
		return errors.New("TEST ERROR")
	})
	if err == nil {
		t.Fatalf("expected error; got nil")
	}
}

func TestInvalidArticle(t *testing.T) {
	t.Parallel()
	err := ta("testdata/invalid_page.xml", func(*Article) error {
		return nil
	})
	if err == nil {
		t.Fatalf("expected error; got nil")
	}
}

func TestInvalidNamespaces(t *testing.T) {
	t.Parallel()
	err := ta("testdata/invalid_namespaces.xml", func(a *Article) error {
		return nil
	})
	if err == nil {
		t.Fatalf("expected error; got nil")
	}
}

func TestNamespaces(t *testing.T) {
	t.Parallel()
	err := ta("testdata/test.xml", func(a *Article) error {
		if a.Namespace != "NS1" {
			t.Fatalf("expected %q; got %q", "NS1", a.Namespace)
		}
		return nil
	})
	if err != nil {
		t.Fatalf("got error: %s", err)
	}
}

func TestTitle(t *testing.T) {
	t.Parallel()
	err := ta("testdata/test.xml", func(a *Article) error {
		if a.Title != "TITLE" {
			t.Fatalf("expected %q; got %q", "TEST", a.Title)
		}
		return nil
	})
	if err != nil {
		t.Fatalf("got error: %s", err)
	}
}

func TestID(t *testing.T) {
	t.Parallel()
	err := ta("testdata/test.xml", func(a *Article) error {
		if a.ID != "ID" {
			t.Fatalf("expected %q; got %q", "ID", a.ID)
		}
		return nil
	})
	if err != nil {
		t.Fatalf("got error: %s", err)
	}
}

func TestModel(t *testing.T) {
	t.Parallel()
	err := ta("testdata/test.xml", func(a *Article) error {
		if a.Model != "wikitext" {
			t.Fatalf("expected %q; got %q", "wikitext", a.Model)
		}
		return nil
	})
	if err != nil {
		t.Fatalf("got error: %s", err)
	}
}

func TestMimeType(t *testing.T) {
	t.Parallel()
	err := ta("testdata/test.xml", func(a *Article) error {
		if a.MimeType != "text/x-wiki" {
			t.Fatalf("expected %q; got %q", "text/x-wiki", a.MimeType)
		}
		return nil
	})
	if err != nil {
		t.Fatalf("got error: %s", err)
	}
}

func TestContent(t *testing.T) {
	const want = "Alan Smithee Allen Smithee"
	t.Parallel()
	err := ta("testdata/test.xml", func(a *Article) error {
		_, err := a.Content.Parse()
		if err != nil {
			t.Errorf("got error: %s", err)
		}
		if !a.Content.Parsed() {
			t.Errorf("content not parsed")
		}
		if got := strings.Trim(a.Content.String(), " \n\t"); got != want {
			t.Errorf("expected %q; got %q", want, got)
		}
		return nil
	})
	if err != nil {
		t.Fatalf("got error: %s", err)
	}
}

func TestNamespaceFind(t *testing.T) {
	t.Parallel()
	ns := namespaces{[]namespace{
		namespace{Key: "A", Value: "A"},
		namespace{Key: "B", Value: "B"},
	}}
	tests := []struct {
		key string
		ok  bool
	}{
		{"A", true},
		{"B", true},
		{"C", false},
	}
	for _, tc := range tests {
		t.Run(tc.key, func(t *testing.T) {
			n, ok := ns.find(tc.key)
			if ok != tc.ok {
				t.Fatalf("expected %t; got %t", tc.ok, ok)
			}
			if !tc.ok {
				return
			}
			if n.Key != tc.key {
				t.Fatalf("expected %q; got %q", tc.key, n.Key)
			}
		})
	}
}

func findArticleByID(file, id string) *Content {
	var content *Content
	err := ta(file, func(a *Article) error {
		if a.ID == id {
			content = a.Content
		}
		return nil
	})
	if err != nil {
		panic("cannot find article " + id + ": " + err.Error())
	}
	return content
}

func TestRedirect(t *testing.T) {
	tests := []struct {
		id, target string
		redirect   bool
	}{
		{"A", "", false},
		{"B", "A", true},
		{"C", "A", true},
		{"D", "A", true},
	}
	for _, tc := range tests {
		t.Run(tc.id, func(t *testing.T) {
			content := findArticleByID("testdata/special.xml", tc.id)
			target, redirect := content.Redirect()
			if target != tc.target {
				t.Fatalf("expected target %q; got %q", tc.target, target)
			}
			if redirect != tc.redirect {
				t.Fatalf("expected redirect %t; got %t", tc.redirect, redirect)
			}
		})
	}
}

func TestCategories(t *testing.T) {
	tests := []struct {
		id   string
		cats []string
	}{
		{"A", []string{}},
		{"E", []string{"Erster Weltkrieg", "Liste", "List", "Author", "Литва"}},
	}
	for _, tc := range tests {
		t.Run(tc.id, func(t *testing.T) {
			content := findArticleByID("testdata/special.xml", tc.id)
			cats := content.Categories()
			for _, cat := range cats {
				var found bool
				for _, want := range tc.cats {
					if want == string(cat) {
						found = true
					}
				}
				if !found {
					t.Fatalf("invalid category: %s", cat)
				}
			}
			for _, want := range tc.cats {
				if !cats.Get(want) {
					t.Fatalf("cannot find %q", want)
				}
			}
		})
	}
}

func TestArticles(t *testing.T) {
	tests := []struct {
		id   string
		arts []string
		n    int
	}{
		{"A", []string{}, 0},
		{"E", []string{}, 0},
		{"F", []string{"Article A", "Article B"}, 2},
	}
	for _, tc := range tests {
		t.Run(tc.id, func(t *testing.T) {
			content := findArticleByID("testdata/special.xml", tc.id)
			arts := content.Articles()
			if got := len(arts); got != tc.n {
				t.Fatalf("expected %d; got %d", tc.n, got)
			}
			for _, art := range tc.arts {
				if !arts.Get(art) {
					t.Fatalf("cannot find %q", art)
				}
			}
		})
	}
}
