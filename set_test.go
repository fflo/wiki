package wiki

import (
	"bytes"
	"testing"
)

func newSet() Set {
	return [][]byte{[]byte("a-cat"), []byte("b-cat"), []byte("c-cat")}
}

func TestSetGet(t *testing.T) {
	tests := []struct {
		cat      string
		contains bool
	}{
		{"a-cat", true},
		{"b-cat", true},
		{"c-cat", true},
		{"d-cat", false},
	}
	for _, tc := range tests {
		t.Run(tc.cat, func(t *testing.T) {
			c := newSet()
			if got := c.Get(tc.cat); got != tc.contains {
				t.Fatalf("expected %t; got %t", tc.contains, got)
			}
		})
	}
}

func TestSetEach(t *testing.T) {
	c := newSet()
	var n int
	c.Each(func(cat []byte) {
		n++
	})
	if n != 3 {
		t.Fatalf("expected %d; got %d", 3, n)
	}
}

func TestSetGetIf(t *testing.T) {
	tests := []struct {
		cat      []byte
		contains bool
	}{
		{[]byte("a-cat"), true},
		{[]byte("b-cat"), true},
		{[]byte("c-cat"), true},
		{[]byte("d-cat"), false},
	}
	for _, tc := range tests {
		t.Run(string(tc.cat), func(t *testing.T) {
			f := func(cat []byte) bool { return bytes.Equal(cat, tc.cat) }
			c := newSet()
			if got := c.GetIf(f); got != tc.contains {
				t.Fatalf("expected %t; got %t", tc.contains, got)
			}
		})
	}
}
