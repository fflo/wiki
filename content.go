package wiki

import (
	"bytes"
	"encoding/xml"
	"io"
	"regexp"
	"strings"

	"github.com/pkg/errors"
)

// IgnoreWikiTags sets tags whose content is ignored
// while text parsing
var IgnoreWikiTags = map[string]struct{}{
	"ref":  {},
	"math": {},
}

// Content represents the HTML formatted main body of a
// wikipedia article. Initially Content only contains the raw (unparsed)
// bytes of the article.
// You have to call Parse() to get the parsed content.
type Content struct {
	text string
	raw  []byte
}

// String implements the Stringer interface.
// If the raw content has not been parsed,
// a copy of the unparsed raw content is returned.
func (c *Content) String() string {
	if c.raw != nil {
		return string(c.raw)
	}
	return c.text
}

// Parsed returns true iff the raw xml content has been parsed.
func (c *Content) Parsed() bool {
	return c.raw == nil
}

// Parse parses the raw content.
// It is secure to call Parse() multiple times.
func (c *Content) Parse() (string, error) {
	if c.raw == nil {
		return c.text, nil
	}
	r, err := copyText(c.raw)
	if err != nil {
		return "", errors.Wrapf(err, "cannot parse content")
	}
	text, err := parseText(r)
	if err != nil {
		return "", errors.Wrapf(err, "cannot parse content")
	}
	c.text = text
	c.raw = nil
	return c.text, nil
}

var redirect = regexp.MustCompile(`(?i)^#(redirect|weiterleitung).*\[\[(.*?)\]\]`)

// Redirect returns if the content marks a redirect
// and where it redirects to. Must only be called on
// unparsed content.
func (c *Content) Redirect() (string, bool) {
	m := redirect.FindSubmatch(c.raw)
	if m == nil {
		return "", false
	}
	return string(m[2]), true
}

var categories = regexp.MustCompile(`(?i)\[\[:?(category|kategorie|категория):(.*?)\]\]`)

// Categories returns the set of categories for the given content.
// Must only be called on uparsed content.
func (c *Content) Categories() Set {
	m := categories.FindAllSubmatch(c.raw, -1)
	if m == nil {
		return nil
	}
	var set Set
	for _, sm := range m {
		for _, cat := range bytes.Split(sm[2], []byte("|")) {
			set = appendTrimmed(set, cat)
		}
	}
	return set
}

var (
	articles       = regexp.MustCompile(`\[\[(.*?)\]\]`)
	specialArticle = regexp.MustCompile(`^\S+:.+$`)
)

// Articles returns the set of articles for the given content.
// Must only be called on uparsed content.
func (c *Content) Articles() Set {
	m := articles.FindAllSubmatch(c.raw, -1)
	if m == nil {
		return nil
	}
	var set Set
	for _, sm := range m {
		if specialArticle.Match(sm[1]) {
			continue
		}
		set = appendTrimmed(set, bytes.SplitN(sm[1], []byte("|"), 2)[0])
	}
	return set
}

// UnmarshalXML implements the xml.Unmarshaller interface.
// It parses the data of a <text> element as html formatet text.
func (c *Content) UnmarshalXML(d *xml.Decoder, start xml.StartElement) error {
	t, err := d.Token()
	if err == io.EOF {
		return nil
	}
	if err != nil {
		return errors.Wrapf(err, "cannot read wiki <text> element")
	}
	switch token := t.(type) {
	case xml.CharData:
		if err != nil {
			return errors.Wrapf(err, "cannot convert raw wiki text")
		}
		c.raw = token.Copy()
	}
	return d.Skip()
}

func parseText(r io.Reader) (string, error) {
	builder := &strings.Builder{}
	d := newContentDecoder(r)
	for {
		t, err := d.Token()
		if err == io.EOF {
			return builder.String(), nil
		}
		if err != nil {
			return "", errors.Wrapf(err, "invalid wiki markup")
		}
		switch token := t.(type) {
		case xml.StartElement:
			if _, ok := IgnoreWikiTags[token.Name.Local]; ok {
				if err := d.Skip(); err != nil {
					return "", errors.Wrapf(err, "invalid wiki markup")
				}
			}
		case xml.CharData:
			builder.Grow(len(token))
			if _, err := builder.Write(token); err != nil {
				return "", errors.Wrapf(err, "invalid wiki markup")
			}
		}
	}
}

func newContentDecoder(r io.Reader) *xml.Decoder {
	d := xml.NewDecoder(r)
	d.Strict = false
	d.AutoClose = xml.HTMLAutoClose
	d.Entity = xml.HTMLEntity
	return d
}

const (
	startTag = "<text>" + "\n"
	endTag   = "</text>" + "\n"
)

func copyText(text []byte) (io.Reader, error) {
	c := &textCopyer{buffer: &bytes.Buffer{}}
	c.writeTag(xml.Header)
	c.writeTag(startTag)
	c.writeContent(text)
	c.writeTag(endTag)
	return c.buffer, c.err
}

type textCopyer struct {
	buffer *bytes.Buffer
	err    error
}

func (t *textCopyer) writeTag(str string) {
	if t.err != nil {
		return
	}
	_, err := t.buffer.WriteString(str)
	if err != nil {
		t.err = err
	}
}

func (t *textCopyer) writeContent(bs []byte) {
	if t.err != nil {
		return
	}
	_, err := t.buffer.Write(cleanup(bs))
	if err != nil {
		t.err = err
	}
}

var (
	startComment = regexp.MustCompile(`<!---*`)
	endComment   = regexp.MustCompile(`-*-->`)
)

func cleanup(bs []byte) []byte {
	bs = startComment.ReplaceAll(bs, []byte("<!--"))
	bs = endComment.ReplaceAll(bs, []byte("-->"))
	return bs
}
