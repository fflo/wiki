package wiki

import (
	"encoding/xml"
	"io"

	"github.com/pkg/errors"
)

// Article represents an article in a wikipedia dump.
type Article struct {
	Title     string   `xml:"title"`           // Title of the wikipedia article.
	ID        string   `xml:"id"`              // ID of the wikipedia article.
	Namespace string   `xml:"ns"`              // Namespace of the wikipedia article; "" if it has no namespace.
	Model     string   `xml:"revision>model"`  // Model of the wikipedia article.
	MimeType  string   `xml:"revision>format"` // MimeType of the wikipedia article.
	Content   *Content `xml:"revision>text"`   // Content of the wikipedia article.
}

// EachArticle parses an XML wikipedia dump and calls the given
// callback for each encountered article. If the callback function
// returns io.EOF, the error is ignored and the iteration stops.
func EachArticle(r io.Reader, f func(*Article) error) error {
	h := &handle{d: xml.NewDecoder(r), f: f}
	for {
		t, e := h.d.Token()
		if e == io.EOF {
			return nil
		}
		if e != nil {
			return errors.Wrapf(e, "cannot parse wiki dump")
		}
		switch token := t.(type) {
		case xml.StartElement:
			switch token.Name.Local {
			case "page":
				if err := h.page(&token); err != nil {
					if err == io.EOF {
						return nil
					}
					return errors.Wrapf(err, "cannot parse page")
				}
			case "namespaces":
				if err := h.namespaces(&token); err != nil {
					return errors.Wrapf(err, "cannot parse namespaces")
				}
			}
			// case xml.EndElement:
			// case xml.CharData:
		}
	}
	// panic("unreacheable")
}

type handle struct {
	d  *xml.Decoder
	ns namespaces
	f  func(*Article) error
}

func (h *handle) page(s *xml.StartElement) error {
	d := xml.NewTokenDecoder(h.d)
	a := &Article{}
	err := d.DecodeElement(a, s)
	if err != nil && err != io.EOF { // ignore io.EOF -> empty content
		return errors.Wrapf(err, "cannot parse wiki article")
	}
	// fix namespace
	if a.Namespace != "" {
		// empty namespace is ok
		n, _ := h.ns.find(a.Namespace)
		a.Namespace = n.Value
	}
	// handle io.EOF and other errors from callback
	err = h.f(a)
	if err == io.EOF {
		return io.EOF
	}
	return errors.Wrapf(err, "invalid wiki article")
}

func (h *handle) namespaces(s *xml.StartElement) error {
	d := xml.NewTokenDecoder(h.d)
	return d.DecodeElement(&h.ns, s)
}

type namespaces struct {
	Namespaces []namespace `xml:"namespace"`
}

type namespace struct {
	Key   string `xml:"key,attr"`
	Kase  string `xml:"case,attr"`
	Value string `xml:",innerxml"`
}

func (ns namespaces) find(key string) (namespace, bool) {
	for _, n := range ns.Namespaces {
		if key == n.Key {
			return n, true
		}
	}
	return namespace{}, false
}
